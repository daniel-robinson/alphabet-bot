export default {
  generateText: (text: string) => {
    let color: 'white' | 'yellow' = 'white';

    text = text.replace(/#[a-zA-Z]+\s/, (match: string) => {
      if (text.startsWith(match)) {
        if (match.toLowerCase() === '#yellow ') {
          color = 'yellow';
        }

        return '';
      }

      return match;
    });

    text = text.replace(/\:.*?\:|\@.*?\s|([a-zA-Z?#!@])/gm, (match: string) => {
      if (match.length > 1) {
        return match;
      }

      if (match === '?') {
        return `:alphabet-${ color }-question:`;
      }

      if (match === '!') {
        return `:alphabet-${ color }-exclamation:`;
      }

      if (match === '@') {
        return `:alphabet-${ color }-at:`;
      }

      if (match === '#') {
        return `:alphabet-${ color }-hash:`;
      }

      return `:alphabet-${ color }-${ match }:`;
    });

    return text;
  },
};