import { APIGatewayProxyEvent, APIGatewayProxyHandler, Context } from 'aws-lambda';
import { parse } from 'querystring';
import 'source-map-support/register';
import { SlashCommand } from './src/models/slash-command';
import alphabetiser from './src/services/alphabetiser';
import slackMessager from './src/services/slack-messager';

export const hello: APIGatewayProxyHandler = async (event, _context) => {
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Go Serverless Webpack (Typescript) v1.0! Your function executed successfully!',
      input: event,
    }, null, 2),
  };
}

export const alphabetise: APIGatewayProxyHandler = async (event: APIGatewayProxyEvent, _context: Context) => {
  const command = (parse(event.body) as unknown) as SlashCommand;
  const text = alphabetiser.generateText(command.text);

  try {
    await slackMessager.message(command, text);
  } catch (error: any) {
    console.error(error);
  }

  return {
    statusCode: 200,
    body: '',
  };
}
